#include "cmath"
#include "kf/kf_log.h"
#include "game.h"

using namespace std;

int main()
{
	Game::inst().run();

	return 0;
}
