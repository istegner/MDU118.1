#include "Game.h"
//#include "rock.h"

Game::Game() : App()
{
}

Game::~Game()
{
}

Game &Game::inst()
{
	static Game s_instance;
	return s_instance;
}

bool Game::start()
{
	/*Rock *rock = qgf::World::build<Rock>();
	rock->position(kf::Vector2(200, 200));
	qgf::World::gravity(kf::Vector2(0, 100));*/

	m_map.atlas = qgf::TextureManager::getSprite("data/tiles.png");
	m_map.SetMapSize(16, 16);

	//TODO: Spawn player

	return true;
}

void Game::update(float deltaT)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		m_running = false;
	}

	//If mouse is not over UI
	if (!ImGui::GetIO().WantCaptureMouse) {
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			//Not pressing UI element
			sf::Vector2i pos = sf::Mouse::getPosition(m_window);
			int tileWidth = 32;
			int tileHeight = 32;
			int tileX = pos.x / tileWidth;
			int tileY = pos.y / tileHeight;
			m_map.SetTile(m_currentTile, tileX, tileY);
		}
	}

	ImGui::Begin("Kage2D");
	ImGui::Columns(3, 0, false);

	if (ImGui::Button("Save")) {
		Save();
	}
	ImGui::NextColumn();
	
	if (ImGui::Button("Load")) {
		Load();
	}
	ImGui::NextColumn();

	if (ImGui::Button("Clear")) {
		Clear();
	}
	ImGui::NextColumn();

	if (ImGui::Button("Exit"))
	{
		m_running = false;
	}
	ImGui::NextColumn();

	//Draw button with sprite
	for (int i = 0; i < 5; i++) {
		qgf::selectSpriteTile1D(m_map.atlas, i, 32, 32);
		ImGui::PushID(i);
		if (ImGui::ImageButton(*m_map.atlas)) {
			m_currentTile.tileIndex = i;
		}
		ImGui::PopID();
		ImGui::NextColumn();
	}
	ImGui::End();
}

void Game::render()
{
	//sf::Vector2i pos = sf::Mouse::getPosition(m_window);
	//qgf::drawCircle(m_window, pos, 100, 32);

	m_map.Render(&m_window);

	if (playMode) {
		sf::View view;
		float aspect = (float)m_window.getSize().x / (float)m_window.getSize().y;
		view.setSize(512, 512 / aspect);
		//view.setCenter(m_enteties[0]->sprite->GetPosition());
		m_window.setView(view);
	}
}

void Game::cleanup()
{
}

void Game::Load()
{
	std::fstream file;
	file.open("data/map.csv", std::ios::in);
	int x;
	int y;
	file >> x;
	file.ignore(1, ', ');
	file >> y;
	m_map.SetMapSize(x, y);
	for (int j = 0; j < y; j++) {
		for (int i = 0; i < x; i++) {
			Tile tempTile;
			file >> tempTile.tileIndex;
			file.ignore(1, ', ');
			file >> tempTile.blocking;
			file.ignore(1, ',');
			m_map.SetTile(tempTile, i, j);
		}
	}
};

void Game::Save()
{
	std::fstream file;
	file.open("data/map.csv", std::ios::out);
	file << m_map.mapWidth << ", " << m_map.mapHeight << std::endl;
	for (int y = 0; y < m_map.mapHeight; ++y)
	{
		for (int x = 0; x < m_map.mapWidth; ++x)
		{
			Tile t = m_map.GetTile(x, y);
			file << t.tileIndex << ", " << t.blocking;
			if (x != m_map.mapWidth - 1)
				file << ", ";
		}
		file << std::endl;
	}
};

void Game::Clear() {
	Tile

	for (int y = 0; y < m_map.mapHeight; ++y)
	{
		for (int x = 0; x < m_map.mapWidth; ++x)
		{
			m_map.SetTile()
		}
	}
}