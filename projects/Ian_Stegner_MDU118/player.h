#pragma once
#include "vector"
#include "map.h"
#include "Game.h"

class Player {
public:
	void MoveUp(Map *map, Tile PlayerTile);
	void MoveDown(Map *map, Tile PlayerTile);
	void MoveLeft(Map *map, Tile PlayerTile);
	void MoveRight(Map *map, Tile PlayerTile);
};