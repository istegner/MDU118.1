#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics.hpp>
#include <qgf2d/imgui.h>
#include <qgf2d/imgui-SFML.h>
#include <qgf2d/sfml_util.h>
#include "kf/kf_log.h"
#include "kf/kf_vector2.h"
#include "qgf2d\gameobject.h"
#include "qgf2d\world.h"
#include "qgf2d\texture_manager.h"


class App
{
protected:
	sf::RenderWindow m_window;
	sf::Clock m_clock;
	sf::Font m_font;
	bool m_running;

public:
	App();
	virtual ~App();
	virtual bool init();
	virtual bool start();
	virtual void update(float deltaT);
	virtual void render();
	virtual void run();
	virtual void cleanup();

	sf::RenderWindow &getWindow();
	sf::Clock &getClock();

};
