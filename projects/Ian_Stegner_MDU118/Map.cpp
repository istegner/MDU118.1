#pragma once
#include "map.h"
#include "qgf2d\sfml_util.h"

void Map::SetMapSize(int width, int height) {
	tiles.resize(width*height);
	mapWidth = width;
	mapHeight = height;
}

void Map::Render(sf::RenderWindow* window) {
	for (int y = 0; y < mapHeight; y++) {
		for (int x = 0; x < mapWidth; x++) {
			auto tile = tiles[x + y * mapWidth];
			qgf::selectSpriteTile1D(atlas, tile.tileIndex, 32, 32);
			atlas->setPosition(x * 32, y * 32);
			window->draw(*atlas);
		}
	}
}

void Map::SetTile(Tile tile, int x, int y) {
	if (x < mapWidth && y < mapHeight) {
		tiles[x + y * mapWidth] = tile;
	}
}

void Map::FindPlayerInMap(Map map, Tile PlayerTile) {

}

Tile Map::GetTile(int x, int y) {
	if (x < mapWidth && y < mapHeight) {
		return tiles[x + y * mapWidth];
	}
	return Tile();
}