#pragma once

#include "app.h"

class Rock : public qgf::GameObject
{
public:
	Rock();
	virtual ~Rock();
	virtual void update(float deltaT);
	virtual void render(sf::RenderWindow &rw);
};
