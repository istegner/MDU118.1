#pragma once

#include "app.h"

class Example : public App
{
public:
	Example();
	virtual ~Example();
	virtual bool start();
	virtual void update(float deltaT);
	virtual void render();
	virtual void cleanup();
	static Example &inst();

	sf::Texture *m_backgroundTexture;
	sf::Sprite *m_backgroundSprite;
};
