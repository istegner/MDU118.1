#include "rock.h"

Rock::Rock(): qgf::GameObject()
{
	m_sprite = qgf::TextureManager::getSprite("data/rock.png");
	m_physicsStyle = qgf::GameObject::PhysicsStyle::e_psNewtonian;
	m_life = 3.0f;
}

Rock::~Rock()
{
}

void Rock::update(float deltaT)
{
	qgf::GameObject::update(deltaT);
}

void Rock::render(sf::RenderWindow &rw)
{
	qgf::GameObject::render(rw);
}


