#include "example.h"
#include "rock.h"

Example::Example(): App()
{
}

Example::~Example()
{
}

Example &Example::inst()
{
	static Example s_instance;
	return s_instance;
}

bool Example::start()
{
	Rock *rock = qgf::World::build<Rock>();
	rock->position(kf::Vector2(200, 200));
	qgf::World::gravity(kf::Vector2(0, 100));

	return true;
}

void Example::update(float deltaT)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		m_running = false;
	}

	ImGui::Begin("Kage2D");
	if(ImGui::Button("Exit"))
	{ 
		m_running = false;
	}
	ImGui::End();
}

void Example::render()
{
	sf::Vector2i pos = sf::Mouse::getPosition(m_window);
	qgf::drawCircle(m_window, pos, 100, 32);
}

void Example::cleanup()
{
}

